package ru.t1.skasabov.tm.exception.field;

public final class SortIncorrectException extends AbstractFieldException {

    public SortIncorrectException() {
        super("Error! Sort is incorrect...");
    }

}
