package ru.t1.skasabov.tm.api.service;

import ru.t1.skasabov.tm.enumerated.Sort;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    void add(Task task);

    void changeTaskStatusById(String id, Status status);

    void changeTaskStatusByIndex(Integer index, Status status);

    Task create(String name, String description);

    Task create(String name);

    void updateById(String id, String name, String description);

    void updateByIndex(Integer index, String name, String description);

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    List<Task> findAll(Sort sort);

    void clear();

    List<Task> findAllByProjectId(String projectId);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void remove(Task task);

    void removeById(String id);

    void removeByIndex(Integer index);

}
