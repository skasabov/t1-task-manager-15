package ru.t1.skasabov.tm.api.service;

import ru.t1.skasabov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
