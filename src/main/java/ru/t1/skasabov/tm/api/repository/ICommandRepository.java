package ru.t1.skasabov.tm.api.repository;

import ru.t1.skasabov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
